import Vue from "vue";
import Button from "./Button.vue";
import Banner from "./Banner.vue";

const Components = {
  Button,
  Banner,
};

Object.keys(Components).forEach((name) => {
  Vue.component(name, Components[name]);
});

export default Components;
